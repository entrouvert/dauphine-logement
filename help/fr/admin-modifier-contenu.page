<page xmlns="http://projectmallard.org/1.0/"
      type="topic" style="task"
      id="admin-modifier-contenu" xml:lang="fr">

<info>
  <link type="guide" xref="admin-gestionnaires"/>
  <desc>Pour modifier les textes existant sur le site ou en ajouter de nouveaux.</desc>
  <revision pkgversion="1.0" version="1.0" date="2012-04-30" status="final"/>
  <credit type="author">
    <name>Pierre Cros</name>
    <email>pcros@entrouvert.com</email>
  </credit>
</info>

<title>Modifier le contenu du site</title>

<section id="modifier-texte">
  <title>Modifier un texte</title>
  <p>Pour modifier un texte du site il suffit, après authentification, de double-cliquer sur ce dernier. Cela provoque l'ouverture d'un éditeur en ligne.</p>

<figure>
  <title>Éditeur WYSIWIG</title>
  <desc>Éditeur de texte permettant la modification des textes du site.</desc>
  <media type="image" mime="image/png" src="figures/editeur.png" >
  </media>
</figure>

<note>
  <p>
Les opérations de copier / coller charrient parfois avec elles des données parasites. Pour copie /coller depuis un traitement de texte comme word par exemple, commencez par enregistrer votre document au format .txt et ouvrez le fichier obtenu avec le notepad. Vous pouvez alors faire votre copier / coller sans risque et reprendre la mise en forme.
  </p>
</note>

<p>L'éditeur en ligne ne peut pas offrir la même souplesse qu'un éditeur de texte bureautique mais il permet tout de même de faire les opérations de base pour structurer correctement un document : titres, gras, italique, listes, liens, images, tableaux.</p>

<note style="warning">
  <p>Il faut impérativement se limiter à l'utilisation des styles disponibles dans l'éditeur pour structurer un document. Cela permet d'avoir une mise en page homogène sur le site.</p>
</note>  

</section>

<section id="creer-page">
  <title>Créer ou supprimer une page</title>
  <p>Lors que l'on est loggué en tant que gestionnaire et que l'on navigue sur les pages publiques de l'application (c'est à dire pas dans l'interface d'administration), on dispose d'une colonne spécifique sur la gauche, qui permet de réorganiser le contenu.</p>

<figure>
  <title>Colonne de gestion du contenu</title>
  <desc>Cette colonne expose les différentes pages du site et permet d'agir sur elles.</desc>
  <media type="image" mime="image/png" src="figures/colonne-edition.png" >
  </media>
</figure>

<p>Les pages sont organisées en deux grandes catégories :</p>
<list>
  <item><p>« Visibles » qui rassemble les pages de contenu normales</p></item>
  <item><p>« Footer » qui rassemble les pages dont le lien figure en pied de page et dont le contenu est peu susceptible d'évoluer.</p></item>
</list>

<p>La création ou la suppression d'une page s'effectue grâce à un clic droit sur une page ou une catégorie. Un menu contextuel apparaît alors qui permet d'ajouter une page à l'endroit choisi ou de supprimer l'élément sélectionné.</p>

<figure>
  <title>Menu contextuel d'édition</title>
  <desc>Menu permettant d'éditer, de supprimer ou de créer une page.</desc>
  <media type="image" mime="image/png" src="figures/menu-contextuel.png" >
  </media>
</figure>

</section>

</page>
