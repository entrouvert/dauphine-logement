<page xmlns="http://projectmallard.org/1.0/"
      type="topic" style="task"
      id="recherche-enregistrer-annonces-interressantes"  xml:lang="fr">

<info>
  <link type="guide" xref="recherche-rechercher-un-logement"/>
  <desc>Permet de sauvegarder les annonces sélectionnées sur la plate-forme.</desc>
  <revision pkgversion="1.0" version="1.0" date="2012-04-30" status="final"/>
  <credit type="author">
    <name>Pierre Cros</name>
    <email>pcros@entrouvert.com</email>
  </credit>
</info>

<title>Enregistrer les annonces intéressantes</title>

<p>Il est possible, lorsque l'on souhaite conserver une annonce, de l'enregistrer pour pouvoir la retrouver aisément sur la plate-forme ultérieurement. Il suffit pour cela de cliquer sur le bouton « Sauvegarder dans mes annonces » disponible en bas de l'annonce.</p>

<figure>
  <title>Sauvegarder une annonce</title>
    <media type="image" mime="image/png" src="figures/bouton-sauvegarder-annonce.png" height="28">
  </media>
</figure>

<p>Pour retrouver l'annonce en question, il faut cliquer sur le lien « Mes annonces » dans la colonne de gauche. Ce lien permet de lister, modifier, effacer les annonces sauvegardées.</p>

<figure>
  <title>Liste des annonces sauvegardées</title>
  <desc>Cette page liste l'ensemble des annonces sauvegardées par l'utilisateur. Le bouton « Supprimer de mes annonces », en bas de l'annonce, permet d'effacer une annonce de la liste.</desc>
  <media type="image" mime="image/png" src="figures/liste-annonces-sauvegardees.png" >
  </media>
</figure>

</page>
