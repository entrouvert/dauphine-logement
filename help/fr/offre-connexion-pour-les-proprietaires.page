<page xmlns="http://projectmallard.org/1.0/"
      type="topic" style="task"
      id="offre-connexion-pour-les-proprietaires"  xml:lang="fr">

<info>
  <link type="guide" xref="offre-proposer-un-logement" group="#first"/>
  <desc>Procédure de connexion pour les propriétaires.</desc>
  <revision pkgversion="1.0" version="1.0" date="2012-04-30" status="final"/>
  <credit type="author">
    <name>Pierre Cros</name>
    <email>pcros@entrouvert.com</email>
  </credit>
</info>

<title>Connexion pour les propriétaires</title>

<p>Les propriétaires disposant d'une compte se connectent à la plate-forme en remplissant le formulaire de login présent sur la page d'accueil. Ils doivent y saisir leur email et leur mot de passe puis cliquer sur le bouton « Ok ».</p>

<figure>
  <title>Formulaire de login</title>
  <desc>Saisir ici les identifiants</desc>
  <media type="image" mime="image/png" src="figures/formulaire-de-login.png" >
  </media>
</figure>

<note>
  <p>
    Cliquez sur le bandeau « Je suis propriétaire, j'accède à mon compte » pour avoir davantage d'informations sur la procédure.
  </p>
</note>

<p>Les propriétaires qui ne disposent pas encore d'un compte peuvent s'en créer un en suivant le lien « Vous êtes propriétaire et vous n'avez pas de compte ? Inscrivez-vous ici ! » présent à côté du formulaire de login. Ils doivent alors compléter le formulaire suivant :</p>

<figure>
  <title>Création de compte</title>
  <desc>Formulaire de création de compte</desc>
  <media type="image" mime="image/png" src="figures/creation-compte.png" >
  </media>
</figure>

<p>Les propriétaires qui ont perdu leur mot de passe peuvent le récupérer en suivant le lien « J'ai perdu mon mot de passe... » présent à côté du formulaire de login. Ils devront alors saisir leur adresse email et un nouveau mot de passe leur sera envoyé.</p>

</page>
