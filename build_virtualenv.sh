#!/bin/bash

if grep -q proxy /etc/hosts; then
	export http_proxy=http://proxy:3128
	export https_proxy=http://proxy:3128
fi

echo "initialisation virtualenv dans /home/logement/virtualenv"

cd /home/logement
HOME=/home/logement
if [ ! -d virtualenv ]; then
	virtualenv-2.6 virtualenv
fi

echo "creation/mise a jour /home/logement/virtualenv"
cd virtualenv
. bin/activate
pip install -U pip
pip install -r /home/logement/src/requirements

# bug dans piston 0.2.3rc1
touch /home/logement/virtualenv/lib/python2.6/site-packages/piston/__init__.py

