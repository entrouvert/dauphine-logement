#!/bin/bash

echo "##"
echo "## installation composants systeme"
echo "##"

echo "installation des paquets logiciels"
if grep -qi centos /etc/redhat-release
then
	DISTRIB=centos
	DISTRIB_NGINX=centos
else
	DISTRIB=redhat
	DISTRIB_NGINX=rhel
fi
echo "  (cible: $DISTRIB)"

if ! rpm -qa | grep -q epel-release-5; then
	echo "  ajout depuis yum EPEL"
	wget http://dl.fedoraproject.org/pub/epel/5/i386/epel-release-5-4.noarch.rpm
	rpm -i epel-release-5-4.noarch.rpm
	rm  epel-release-5-4.noarch.rpm
fi

if ! rpm -qa | grep -q pgdg-${DISTRIB}-8.4; then
	echo "  ajout depot yum postgresql"
	wget http://yum.postgresql.org/8.4/redhat/rhel-5-x86_64/pgdg-${DISTRIB}-8.4-3.noarch.rpm
	rpm -i pgdg-${DISTRIB}-8.4-3.noarch.rpm
	rm pgdg-${DISTRIB}-8.4-3.noarch.rpm
fi

if ! rpm -qa | grep -q rpmforge-release; then
	echo "  ajout depot yum rpmforge"
	wget http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.2-2.el5.rf.x86_64.rpm
	rpm -i rpmforge-release-0.5.2-2.el5.rf.x86_64.rpm
	rm rpmforge-release-0.5.2-2.el5.rf.x86_64.rpm
fi

if [ ! -f /etc/yum.repos.d/nginx.repo ]; then
	echo "  ajout depot yum nginx"
	cat >/etc/yum.repos.d/nginx.repo <<EOF
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/${DISTRIB_NGINX}/5/x86_64/
gpgcheck=0
enabled=1
EOF
fi

echo "installation des paquets"
yum makecache
yum erase postgres-libs httpd
yum install python26 git gcc python26-virtualenv python26-devel nginx \
	postgresql84 postgresql84-server postgis memcached python26-imaging \
	python26-ldap python26-numpy geos-3.1.0 postgis proj gdal-1.4.4 proj-devel \
	postgresql84-devel proj-epsg

echo ""
echo "post-installation"

echo "  gdal: fix missing Google Mercator Projection"
# FIX missing Google Mercator Projection
if ! grep -q 900913 /usr/share/gdal/cubewerx_extra.wkt; then
	cat cubewerx_extra.wkt.addendum >> /usr/share/gdal/cubewerx_extra.wkt
fi

echo "activation et demarrage postgresql"
if [ ! -d /var/lib/pgsql/data/base/ ]; then
	echo "initialisation postgresql"
	/sbin/service postgresql initdb
fi
/sbin/chkconfig --add postgresql
/sbin/chkconfig postgresql on
/sbin/chkconfig --list postgresql
/sbin/service postgresql start

echo "activation et demarrage memcached"
/sbin/chkconfig --add memcached
/sbin/chkconfig memcached on
/sbin/chkconfig --list memcached
/sbin/service memcached start

echo "activation et demarrage nginx"
/sbin/chkconfig --add nginx
/sbin/chkconfig nginx on
/sbin/chkconfig --list nginx
/sbin/service nginx start

echo "##"
echo "## fin installation composants systeme"
echo "##"


