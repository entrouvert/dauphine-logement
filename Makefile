VERSION=0.25

package:
	git archive -o ../appli-logement-$(VERSION).tar --prefix=appli-logement-$(VERSION)/ HEAD
	gzip ../appli-logement-$(VERSION).tar

dump:
	pg_dump -O -c logement >logement.dump.sql
	tar cvzf logement.backup.tar.gz /var/lib/logment/media/ logement.dump.sql

restore:
	tar xvzf logement.backup.tar.gz
	dropdb logement
	createdb logement
	cat logement.dump.sql | psql logement
