#!/bin/sh

cd /home/logement/src
echo "  creation base logement"
sudo -u postgres dropdb logement 2> /dev/null
sudo -u postgres createdb -T template_postgis -O logement logement
echo "  initialisation base logement"
sudo -u logement ../virtualenv/bin/python ./logement-prod syncdb --noinput --migrate
sudo -u logement ../virtualenv/bin/python ./logement-prod loaddata */fixtures/initial_*.json

