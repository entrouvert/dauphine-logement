#!/bin/sh
#
# logement            Startup script for logement
#
# chkconfig: - 85 15
# processname: logement
# pidfile: /var/run/logement.pid
# description: application logement de l'universite  paris dauphine
#
### BEGIN INIT INFO
# Provides: logement
# Required-Start: $local_fs $remote_fs $network
# Required-Stop: $local_fs $remote_fs $network
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: start and stop application logement dauphine
### END INIT INFO

# Source function library.
. /etc/rc.d/init.d/functions
prog=logement
pidfile=${PIDFILE-/var/run/logement/pid}
etc=/etc/logement/
logement="PYTHONPATH=$etc /home/logement/virtualenv/bin/python /home/logement/src/logement-prod runfcgi host=localhost port=8000 method=threaded pidfile=$pidfile"
SLEEPMSEC=100000
RETVAL=0

start() {
    dirpid=`dirname ${pidfile}`
    mkdir -p $dirpid
    chown logement $dirpid

    echo -n $"Starting $prog: "

    daemon --user logement ${logement}
    RETVAL=$?
    echo
    return $RETVAL
}

stop() {
    echo -n $"Stopping $prog: "
    killproc -p ${pidfile} ${prog}
    RETVAL=$?
    echo
    [ $RETVAL = 0 ] && rm -f ${pidfile}
}

reload() {
    echo -n $"Reloading $prog: "
    killproc -p ${pidfile} ${prog} -HUP
    RETVAL=$?
    echo
}

rh_status() {
    status -p ${pidfile} ${nginx}
}

# See how we were called.
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        start
        ;;
    force-reload|reload)
        reload
        ;;
    *)
        echo $"Usage: $prog {start|stop|restart|reload|help}"
        RETVAL=2
esac

exit $RETVAL
