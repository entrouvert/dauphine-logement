#!/bin/bash

echo "arret services"
/sbin/service logement stop
/sbin/service nginx stop
/sbin/service memcached stop
/sbin/service postgresql stop

echo "suppression utilisateur logement"
/usr/sbin/userdel logement
/usr/sbin/groupdel logement

echo "suppression paquets"
yum erase python26 git gcc python26-virtualenv python26-devel nginx \
	postgresql84 postgresql84-server postgis memcached python26-imaging \
	python26-ldap python26-numpy geos-3.1.0 postgis proj gdal-1.4.4 proj-devel

echo "suppression depots yum"
yum erase epel-release pgdg-centos pgdg-redhat rpmforge-release
rm -rf /etc/yum.repos.d/nginx.repo

echo "suppression donnees"
rm -rf /home/logement
rm -f /etc/init.d/logement
rm -rf /var/lib/pgsql
rm -rf /etc/nginx

echo "** fin"
