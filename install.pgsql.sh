#!/bin/bash

echo "##"
echo "## preparation base postgresl"
echo "##"

cd /tmp

if ! sudo -u postgres psql -c \\l | grep -q template_postgis; then
	echo "  creation template postgis"
	sudo -u postgres createdb -E UTF8 template_postgis
	sudo -u postgres createlang -d template_postgis plpgsql
	sudo -u postgres psql -d postgres -c "UPDATE pg_database SET datistemplate='true' WHERE datname='template_postgis';"
	sudo -u postgres psql -d template_postgis -f /usr/share/pgsql/contrib/lwpostgis-64.sql
	sudo -u postgres psql -d template_postgis -f /usr/share/pgsql/contrib/spatial_ref_sys.sql
	sudo -u postgres psql -d template_postgis -c "GRANT ALL ON geometry_columns TO PUBLIC;"
	#sudo -u postgres psql -d template_postgis -c "GRANT ALL ON geography_columns TO PUBLIC;"
	sudo -u postgres psql -d template_postgis -c "GRANT ALL ON spatial_ref_sys TO PUBLIC;"
fi

if ! sudo -u postgres psql -c \\dg | grep -q logement; then
	echo "  creation utilisation logement"
	sudo -u postgres createuser -d -R -S logement
fi

if ! sudo -u postgres psql -c \\l | grep -q logement; then
	echo "  creation base logement"
	sudo -u postgres dropdb logement 2> /dev/null
	sudo -u postgres createdb -T template_postgis -O logement logement
fi

echo "##"
echo "## fin preparation base postgresl"
echo "##"

