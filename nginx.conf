proxy_cache_path /var/cache/nginx/tilecache levels=1:2:2 keys_zone=tilecache:1536m;

server {
    listen       80;
    server_name  localhost;
    client_max_body_size 100m;

    location /static/ {
      alias /var/lib/logement/static/;
    }
    location /favicon.ico {
      alias /var/lib/logement/img/favicon.ico;
    }
    location /media/ {
      alias /var/lib/logement/media/;
    }

    location / {
	fastcgi_pass_header Authorization;
	fastcgi_intercept_errors off;

	fastcgi_param REQUEST_METHOD    $request_method;
	fastcgi_param QUERY_STRING      $query_string;
	fastcgi_param CONTENT_TYPE      $content_type;
	fastcgi_param CONTENT_LENGTH    $content_length;
	fastcgi_param SERVER_PORT       $server_port;
	fastcgi_param SERVER_PROTOCOL   $server_protocol;
	fastcgi_param SERVER_NAME       $server_name;

	fastcgi_param REQUEST_URI       $request_uri;
	fastcgi_param DOCUMENT_URI      $document_uri;
	fastcgi_param DOCUMENT_ROOT     $document_root;
	fastcgi_param SERVER_ADDR       $server_addr;
	fastcgi_param REMOTE_USER       $remote_user;
	fastcgi_param REMOTE_ADDR       $remote_addr;
	fastcgi_param REMOTE_PORT       $remote_port;
	fastcgi_param SERVER_SOFTWARE   "nginx";
	fastcgi_param GATEWAY_INTERFACE "CGI/1.1";

	fastcgi_param UID_SET           $uid_set;
	fastcgi_param UID_GOT           $uid_got;
	fastcgi_pass localhost:8000;
    }
    location /tiles/ {
	proxy_pass http://tile.openstreetmap.org/ ;
	proxy_cache tilecache;
	proxy_cache_key "$scheme$host$request_uri";
	proxy_cache_valid 200 302 300h;
	proxy_cache_valid 404 1m;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
