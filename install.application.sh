#!/bin/bash

echo "##"
echo "## installation application logement dans /home/logement"
echo "##"

if [ ! -d /home/logement ]; then
	echo "(creation utilisateur logement)"
	/usr/sbin/adduser logement
fi

echo "copie du projet dans /home/logement/src/"
rsync -az --delete `pwd`/ /home/logement/src/
chown -R logement.logement /home/logement/src

echo "installation de la crontab"
crontab -u logement cronfile

echo "(installation des paquets pour virtualenv)"
yum install python26 gcc python26-virtualenv python26-devel 

# creation virtualenv
sudo -u logement /home/logement/src/build_virtualenv.sh

if [ ! -d /etc/logement ]; then
	mkdir /etc/logement
	chown logement.logement /etc/logement
fi
cd /etc/logement/
if [ ! -f local_settings_dev.py ]
then
	echo "initialisation de /etc/logement/local_settings_dev.py"
	echo 'STATIC_ROOT = "/home/logement/static/"' >> local_settings_dev.py
	echo 'SECRET_KEY = "'`openssl rand -base64 40`'"' >> local_settings_dev.py
fi
echo "définition des autorisations sur /etc/logement/local_settings_dev.py"
chown logement.logement local_settings_dev.py*
chmod o-r local_settings_dev.py*

cd /home/logement/src
echo "initialisation base projet"
cd appli_project
sudo -u logement ./logement-prod syncdb --noinput --migrate
/home/logement/src/flush_database.sh
sudo -u logement ./logement-prod collectstatic -l --noinput

echo "droits de nginx sur l'application"
/usr/sbin/usermod -a -G logement nginx
chmod g+rX /home/logement
chmod -R g+rX /home/logement/static

cd /home/logement/src
if ! cmp nginx.conf /etc/nginx/conf.d/default.conf
then
	echo "configuration nginx dans /etc/nginx/conf.d/default.conf"
	cp -f `pwd`/nginx.conf /etc/nginx/conf.d/default.conf
	if [ ! -d /var/cache/nginx/tilecache ]; then
		echo "création du répertoire pour le cache osm"
		mkdir -p /var/cache/nginx/tilecache
		chown nginx.nginx /var/cache/nginx/tilecache
	fi
	echo "redemarrage nginx"
	/sbin/service nginx restart

fi

echo 'création des répertoires dans /var'
mkdir -p /var/log/logement /var/lib/logement
chown logement.logement /var/log/logement /var/lib/logement

echo "installation du script de lancement /etc/init.d/logement"
cp init.d /etc/init.d/logement

echo "configuration TLS pour les connections LDAP"
if [ ! -f /home/logement/.ldaprc ] || ! grep -q "TLS_REQCERT never" /home/logement/.ldaprc; then
	echo TLS_REQCERT never >>/home/logement/.ldaprc
fi

echo "activation et (re)demarrage du service logement"
/sbin/chkconfig --add logement
/sbin/chkconfig logement on
/sbin/chkconfig --list logement
/sbin/service logement restart



echo "##"
echo "## fin installation application logement"
echo "##"

