#!/bin/bash

echo "##"
echo "## mise à jour de l'application logement dans /home/logement"
echo "##"

echo "copie du projet dans /home/logement/src/"
rsync -az --delete `pwd`/ /home/logement/src/
chown -R logement.logement /home/logement/src

echo "installation gettext"
yum install gettext

echo 'création des répertoires dans /var'
mkdir -p /var/log/logement /var/lib/logement
chown logement.logement /var/log/logement /var/lib/logement

echo "installation de la crontab"
crontab -u logement cronfile

# creation virtualenv
sudo -u logement /home/logement/src/build_virtualenv.sh

cd /home/logement/src
echo "initialisation base projet"
sudo -u logement /home/logement/virtualenv/bin/python ./logement-prod syncdb --noinput
sudo -u logement /home/logement/virtualenv/bin/python ./logement-prod migrate --delete-ghost-migrations
sudo -u logement /home/logement/virtualenv/bin/python ./logement-prod collectstatic -l --noinput
sudo -u logement /home/logement/virtualenv/bin/python ./logement-prod compilemessages

cd /home/logement/src
if ! cmp nginx.conf /etc/nginx/conf.d/default.conf
then
	echo "configuration nginx dans /etc/nginx/conf.d/default.conf"
	cp -f `pwd`/nginx.conf /etc/nginx/conf.d/default.conf
	if [ ! -d /var/cache/nginx/tilecache ]; then
		echo "création du répertoire pour le cache osm"
		mkdir -p /var/cache/nginx/tilecache
		chown nginx.nginx /var/cache/nginx/tilecache
	fi
	echo "redemarrage nginx"
	/sbin/service nginx restart
fi

echo "installation du script de lancement /etc/init.d/logement"
cp init.d /etc/init.d/logement

echo "activation et (re)demarrage du service logement"
/sbin/service logement restart

echo "##"
echo "## fin de la mise à jour de l'application logement"
echo "##"

