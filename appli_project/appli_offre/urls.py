# vim:spell:spelllang=fr
# -*- encoding: utf-8 -*-

from django.conf.urls.defaults import patterns, url

import views

urlpatterns = patterns('',
        url(r'^$', views.Accueil.as_view(), name='offre'),
        url(r'^connexion/$', views.Connexion.as_view(),
            name='offre-connexion'),
        url(r'^inscription/$', views.Inscription.as_view(),
            name='inscription'),
        url(r'^mon-email/$', views.MonEmail.as_view(),
            name='mon-email'),
        url(r'^mes-alertes/$', views.MesAlertes.as_view(),
            name='mes-alertes-offre'),
        url(r'^nouvelle-annonce/$', views.NouvelleAnnonce.as_view(),
            name='creer-annonce'),
        url(r'^(?P<pk>\d+)/$',
            views.EditerAnnonce.as_view(), name='editer-annonce'),
        url(r'^(?P<pk>\d+)/supprimer/$',
            views.SupprimerAnnonce.as_view(),
            name='supprimer-annonce'),
    )
