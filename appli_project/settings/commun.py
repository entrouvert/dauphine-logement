# -*- encoding: utf-8 -*-

import os.path
import logging.handlers


PROJECT_ROOT = os.path.join(os.path.dirname(__file__), '..')

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'logement',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Applications Django utilisées
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'django.contrib.gis',
    # crispy
    'crispy_forms',
    # pagination
    'pagination',
    # south
    'south',
    'django_journal',
    # filtre sur les dates
    'daterange_filter',
    # appli
    'appli_project.appli_socle',
    'appli_project.appli_offre',
    'appli_project.appli_recherche',
    # fiber apps:
    'mptt',
    'compressor',
    'fiber',
    'modeltranslation',
    'fiber_modeltranslation',
    'rest_framework',
    'rest_framework.authtoken',
)

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

ugettext = lambda s: s
LANGUAGES = (
    ('fr', ugettext('French')),
    ('en', ugettext('English')),
)

LOCALE_PATHS = (
        os.path.join(PROJECT_ROOT, 'locale'),
)

FORMAT_MODULE_PATH = 'appli_project.formats'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
# fiber
    'compressor.finders.CompressorFinder',
)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, '..', 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_ROOT, '..', 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'te9pqmis@-oz8#s2qj#$w!jz0kcmy3hbf3&7+!om-oxek^fiw$'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'fiber.middleware.ObfuscateEmailAddressMiddleware',
    'appli_project.appli_socle.middleware.AdminPageMiddlewareOverloaded',
    'pagination.middleware.PaginationMiddleware',
    'appli_project.appli_socle.middleware.LoggerMiddleware',
    'django_journal.middleware.JournalMiddleware',
)

ROOT_URLCONF = 'appli_project.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_ROOT, "templates"),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
# fiber
    'fiber.context_processors.page_info',
)

CACHES = {
      'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
        'appli_project.appli_socle.backends.ProfilRechercheAuthentification',
        'appli_project.appli_socle.backends.ProfilOffreMoteurAuthentification', )

# Configuration du serveur LDAP pour l'authentification CAS
LDAP_URL = 'ldaps://ldap.ent.dauphine.fr'
LDAP_BIND_DN = 'uid=logement,ou=bindusers,dc=dauphine,dc=fr'
LDAP_BIND_PASSWORD = 'FIXME'
LDAP_BASE = 'dc=dauphine,dc=fr'
LDAP_CAS_UID = 'updLogin'

#
CAS_URL = 'https://passeport.dauphine.fr/cas/'

# Crispy Forms
CRISPY_TEMPLATE_PACK = 'bootstrap'

# Domaine utilisé par défaut pour créer des objets du type UtilisateurCAS, i.e.
# le lien entre un utiisateur Django et un compte CAS/LDAP/ENT
UTILISATEUR_CAS_DOMAINE_PAR_DEFAUT = 'dauphine'
TYPE_OFFRE_PAR_DEFAUT = ( u'Logement classique', )

# Configuration fiber "avancée" (https://github.com/ridethepony/django-fiber/blob/master/docs/advanced-usage.rst)

FIBER_DEFAULT_TEMPLATE = 'base.html'
FIBER_TEMPLATE_CHOICES = ( 
    ('', 'Default template (base.html)'),
)
FIBER_EXCLUDE_URLS = [ 'admin/', 'superadmin/' ]
FIBER_IMAGES_DIR = 'uploads/images'
FIBER_FILES_DIR = 'uploads/files'
SOUTH_MIGRATION_MODULES = { 'fiber': 'appli_project.migrations.fiber', }



# Inscription
PROFIL_OFFRE_NON_CONFIRME_TIMEOUT = 2 # en jours
INSCRIPTION_TIMEOUT = 3600
INSCRIPTION_FROM_EMAIL = 'no-reply@www.logement.dauphine.fr'
CHANGE_MAIL_TIMEOUT = 3600
CONTACT_FROM = INSCRIPTION_FROM_EMAIL
CHANGE_EMAIL_FROM = INSCRIPTION_FROM_EMAIL
ALERTE_FROM = INSCRIPTION_FROM_EMAIL
VALIDATION_FROM = INSCRIPTION_FROM_EMAIL

def LOGGING_FUNC(root, DEBUG):
    return {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'syslog': {
            'format': 'logement(pid=%(process)d) %(levelname)s %(name)s: %(message)s',
        },
        'syslog_debug': {
            'format': 'logement(pid=%(process)d) %(levelname)s %(asctime)s t_%(thread)s %(name)s: %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'filters': [],
        },
        'syslog': {
            'level': 'DEBUG',
            'class': 'entrouvert.logging.handlers.SysLogHandler', 
            'formatter': 'syslog_debug' if DEBUG else 'syslog',
            'facility': logging.handlers.SysLogHandler.LOG_LOCAL0,
            'address': '/dev/log',
            'max_length': 999,
        },
    },
    'loggers': {
        'django.journal': {
            'level': 'INFO',
            'propagate': False,
            'handlers': ['syslog', 'mail_admins'],
        },
        '': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'handlers': ['syslog', 'mail_admins'],
        },
    },
}
