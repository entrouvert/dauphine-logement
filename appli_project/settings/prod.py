# -*- encoding: utf-8 -*-

# Paramètre pour le développement de l'application Django

from commun import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG
INTERNAL_IPS = ('127.0.0.1',)

ADMINS = ()

MANAGERS = ADMINS
LOGGING = LOGGING_FUNC('/var/log/logement', DEBUG)
MEDIA_ROOT = '/var/lib/logement/media/'
STATIC_ROOT = '/var/lib/logement/static/'
INSTALLED_APPS = INSTALLED_APPS + (
    'raven.contrib.django.raven_compat',
)
RAVEN_CONFIG = {
    'dsn': 'https://2c17ac65d58e49b180238e76aa880af1:cb8739da55fd40c4b44750661653edae@sentry.entrouvert.org/12',
}

try:
    from local_settings_prod import *
except ImportError:
    pass
