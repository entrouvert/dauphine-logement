# -*- encoding: utf-8 -*-

# Paramètre pour le développement de l'application Django

from commun import *

INSTALLED_APPS += ('debug_toolbar',)
MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
DEBUG = True
TEMPLATE_DEBUG = DEBUG
INTERNAL_IPS = ('127.0.0.1',)
DEBUG_TOOLBAR_CONFIG = dict(INTERCEPT_REDIRECTS = False)

ADMINS = (
    ('Benjamin Dauvergne', 'bdauvergne@entrouvert.com'),
)

MANAGERS = ADMINS
LOGGING = LOGGING_FUNC(PROJECT_ROOT, DEBUG)

try:
    from local_settings_dev import *
except ImportError:
    pass
