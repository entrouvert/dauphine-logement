# vim:spell:spelllang=fr
# -*- encoding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from django.conf import settings

MAIL_TEMPLATES = {
        'Alerte nouvelles annonces':
        { 'sujet': _(u'{{ annonces|length }} nouvelles annonces vous intéressent sur {{host}}'),

          'texte': _(u'''{% load url from future %}Les annonces suivantes correspondent à vos critères:{% for annonce in annonces %}
- {{ host }}{% url 'recherche-annonce' pk=annonce.id %}
{% endfor %}

Pour modifier vos critères allez sur {{host}}{% url 'mes-alertes' %} .
''') }, 
        'Annonce valide':
        { 'sujet': _(u'Votre annonce est publiée'),
          'texte': _(u'''{% load url from future %}L'annonce suivante a été validée pour publication:
{{ annonce.type }} à {{ annonce.prix_par_mois }} € dans {{ annonce.ville }}

Vous pouvez la consulter sur {{host}}{% url 'offre' %} .
''') },
        'Annonce invalide':
        { 'sujet': _(u'Votre annonce est refusée à la publication'),
            'texte': _(u'''{% load url from future %}L'annonce suivante a:

{{ annonce.type }} à {{ annonce.prix_par_mois }} € dans {{ annonce.ville }}

a été refusée à la publication pour la raison:

===================================================
{{raison}}
===================================================

Vous pouvez la modifier sur {{host}}{% url 'editer-annonce' pk=annonce.id %} .
''') },
         'Depublication automatique': {
             'sujet': _(u'''Dépublication automatique d\'une de vos \
annonces sur {{host}}'''),
             'texte': _(u'''{% load url from future %}L'annonce:

    {{ annonce.type }} à {{ annonce.prix_par_mois }} € dans {{ annonce.ville }}

a été dépubliée car elle est publiée depuis plus de {{duree}} jours.
Vous pouvez la republier en vous connectant à votre compte sur

    {{host}}{% url 'editer-annonce' pk=annonce.id %}''') },

          }

MAIL_TEMPLATES.update(getattr(settings, 'MAIL_TEMPLATES', {}))

