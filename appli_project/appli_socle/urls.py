# -*- encoding: utf-8 -*-

from django.conf.urls.defaults import patterns, url
from django.contrib.auth.decorators import login_required

from views import (AcceptationCGU, AcceptationCharteQualite, Deconnexion,
        Homepage, SupprimerMonCompte, VueCodeDeConfirmationEmail,
        VueMotDePassePerdu, ChangementMotDePasse, PlanDuSite)

urlpatterns = patterns('',
        url(r'^$', Homepage.as_view()),
        url(r'^acceptation-cgu/$', login_required(AcceptationCGU.as_view())),
        url(r'^acceptation-charte-qualite/$', login_required(AcceptationCharteQualite.as_view())),
        url(r'^deconnexion/$', Deconnexion.as_view()),
        url(r'^supprimer-mon-compte/$', SupprimerMonCompte.as_view(),
            name='supprimer-compte'),
        url(r'^confirmation-email/(?P<jeton>.*)$', VueCodeDeConfirmationEmail.as_view(),
            name='confirmation-email'),
        url(r'^mot-de-passe-perdu/(?P<email>.*)$', VueMotDePassePerdu.as_view(),
            name='mot-de-passe-perdu'),
        url(r'^changement-mot-de-passe/$', ChangementMotDePasse.as_view(),
            name='changement-mot-de-passe'),
        url(r'^plan-du-site/$',
            PlanDuSite.as_view(),
            name='plan-du-site'),
        )
