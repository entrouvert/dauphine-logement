# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Annonce.duree_location'
        db.add_column(u'appli_socle_annonce', 'duree_location',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Annonce.duree_location'
        db.delete_column(u'appli_socle_annonce', 'duree_location')


    models = {
        u'appli_socle.annonce': {
            'Meta': {'ordering': "('etat_validation', 'derniere_publication')", 'object_name': 'Annonce'},
            'date_de_creation': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'derniere_publication': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'derniere_validation': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'derniere_validation_par': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'duree_location': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'etat_validation': ('django.db.models.fields.CharField', [], {'default': "'attend_validation'", 'max_length': '32', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monnaie': ('django.db.models.fields.CharField', [], {'default': "'EUR'", 'max_length': '3'}),
            'nos_logements': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'pays': ('django.db.models.fields.CharField', [], {'default': "'FR'", 'max_length': '3'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'position_geographique': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'prestations': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['appli_socle.Prestation']", 'symmetrical': 'False', 'blank': 'True'}),
            'prix_par_mois': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2', 'db_index': 'True'}),
            'proprietaire': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'annonces'", 'to': u"orm['appli_socle.ProfilOffre']"}),
            'raison_invalidation': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'surface_en_m2': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'type_d_offre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appli_socle.TypeOffre']", 'on_delete': 'models.SET_DEFAULT'}),
            'type_de_logement': ('django.db.models.fields.CharField', [], {'default': "'studio'", 'max_length': '16'}),
            'ville': ('django.db.models.fields.CharField', [], {'default': "'Paris'", 'max_length': '64'}),
            'zones': ('django.db.models.fields.related.ManyToManyField', [], {'db_index': 'True', 'to': u"orm['appli_socle.Zone']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'appli_socle.emailmodele': {
            'Meta': {'object_name': 'EmailModele'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'sujet': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'texte': ('django.db.models.fields.TextField', [], {})
        },
        u'appli_socle.parametre': {
            'Meta': {'object_name': 'Parametre'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'valeur': ('picklefield.fields.PickledObjectField', [], {}),
            'valeur_email': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appli_socle.EmailModele']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'})
        },
        u'appli_socle.prestation': {
            'Meta': {'object_name': 'Prestation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type_de_prestation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appli_socle.TypeDePrestation']"}),
            'valeur_numerique': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'appli_socle.profiloffre': {
            'Meta': {'object_name': 'ProfilOffre'},
            'acceptation_cgu': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'acceptation_charte_qualite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accepte_invitations': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accepte_notif_alertes': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accepte_notif_depublication': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comment': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'blank': 'True'}),
            'departement': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64', 'blank': 'True'}),
            'sans_validation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url_de_contact': ('django.db.models.fields.URLField', [], {'max_length': '1024', 'blank': 'True'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'}),
            'ville': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'blank': 'True'})
        },
        u'appli_socle.profilrecherche': {
            'Meta': {'object_name': 'ProfilRecherche'},
            'acceptation_cgu': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'acceptation_charte_qualite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'annonces_sauvegardees': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'sauvegardee_par'", 'blank': 'True', 'to': u"orm['appli_socle.Annonce']"}),
            'recherches_sauvegardees': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['appli_socle.RechercheSauvegardee']", 'symmetrical': 'False', 'blank': 'True'}),
            'type_d_offre': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['appli_socle.TypeOffre']", 'symmetrical': 'False', 'blank': 'True'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'appli_socle.recherchesauvegardee': {
            'Meta': {'object_name': 'RechercheSauvegardee'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intra_muros': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prix_maximum': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'types_de_logement': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'zones': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['appli_socle.Zone']", 'null': 'True', 'blank': 'True'})
        },
        u'appli_socle.typedeprestation': {
            'Meta': {'ordering': "('ordre', 'nom')", 'object_name': 'TypeDePrestation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'ordre': ('django.db.models.fields.IntegerField', [], {}),
            'type_de_valeur': ('django.db.models.fields.CharField', [], {'default': "'B'", 'max_length': '1'})
        },
        u'appli_socle.typeoffre': {
            'Meta': {'object_name': 'TypeOffre'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'appli_socle.utilisateurcas': {
            'Meta': {'unique_together': "(('identifiant', 'domain'),)", 'object_name': 'UtilisateurCAS'},
            'domain': ('django.db.models.fields.CharField', [], {'default': "'dauphine'", 'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifiant': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'utilisateur_cas'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'appli_socle.zone': {
            'Meta': {'object_name': 'Zone'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'polygon': ('django.contrib.gis.db.models.fields.PolygonField', [], {})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['appli_socle']