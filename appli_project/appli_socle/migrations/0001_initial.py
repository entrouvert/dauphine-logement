# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TypeOffre'
        db.create_table('appli_socle_typeoffre', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nom', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal('appli_socle', ['TypeOffre'])

        # Adding model 'TypeDePrestation'
        db.create_table('appli_socle_typedeprestation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nom', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('ordre', self.gf('django.db.models.fields.IntegerField')()),
            ('type_de_valeur', self.gf('django.db.models.fields.CharField')(default='B', max_length=1)),
        ))
        db.send_create_signal('appli_socle', ['TypeDePrestation'])

        # Adding model 'ProfilOffre'
        db.create_table('appli_socle_profiloffre', (
            ('user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, primary_key=True)),
            ('acceptation_cgu', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('acceptation_charte_qualite', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('sans_validation', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('accepte_notif_alertes', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('accepte_notif_depublication', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('appli_socle', ['ProfilOffre'])

        # Adding model 'Prestation'
        db.create_table('appli_socle_prestation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type_de_prestation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appli_socle.TypeDePrestation'])),
            ('valeur_numerique', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('appli_socle', ['Prestation'])

        # Adding model 'Zone'
        db.create_table('appli_socle_zone', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nom', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('polygon', self.gf('django.contrib.gis.db.models.fields.PolygonField')()),
        ))
        db.send_create_signal('appli_socle', ['Zone'])

        # Adding model 'Annonce'
        db.create_table('appli_socle_annonce', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('est_publiee', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('derniere_publication', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('derniere_validation', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('date_de_creation', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('validation', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('proprietaire', self.gf('django.db.models.fields.related.ForeignKey')(related_name='annonces', to=orm['appli_socle.ProfilOffre'])),
            ('prix_par_mois', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('monnaie', self.gf('django.db.models.fields.CharField')(default='EUR', max_length=3)),
            ('surface_en_m2', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('nombre_de_pieces', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('pays', self.gf('django.db.models.fields.CharField')(default='FR', max_length=3)),
            ('ville', self.gf('django.db.models.fields.CharField')(default='Paris', max_length=64)),
            ('position_geographique', self.gf('django.contrib.gis.db.models.fields.PointField')()),
            ('type_d_offre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appli_socle.TypeOffre'])),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('appli_socle', ['Annonce'])

        # Adding M2M table for field zones on 'Annonce'
        db.create_table('appli_socle_annonce_zones', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('annonce', models.ForeignKey(orm['appli_socle.annonce'], null=False)),
            ('zone', models.ForeignKey(orm['appli_socle.zone'], null=False))
        ))
        db.create_unique('appli_socle_annonce_zones', ['annonce_id', 'zone_id'])

        # Adding M2M table for field prestations on 'Annonce'
        db.create_table('appli_socle_annonce_prestations', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('annonce', models.ForeignKey(orm['appli_socle.annonce'], null=False)),
            ('prestation', models.ForeignKey(orm['appli_socle.prestation'], null=False))
        ))
        db.create_unique('appli_socle_annonce_prestations', ['annonce_id', 'prestation_id'])

        # Adding model 'RechercheSauvegardee'
        db.create_table('appli_socle_recherchesauvegardee', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('prix_maximum', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('intra_muros', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('quadrant', self.gf('django.db.models.fields.CharField')(max_length=2)),
        ))
        db.send_create_signal('appli_socle', ['RechercheSauvegardee'])

        # Adding model 'ProfilRecherche'
        db.create_table('appli_socle_profilrecherche', (
            ('user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, primary_key=True)),
            ('acceptation_cgu', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('appli_socle', ['ProfilRecherche'])

        # Adding M2M table for field type_d_offre on 'ProfilRecherche'
        db.create_table('appli_socle_profilrecherche_type_d_offre', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('profilrecherche', models.ForeignKey(orm['appli_socle.profilrecherche'], null=False)),
            ('typeoffre', models.ForeignKey(orm['appli_socle.typeoffre'], null=False))
        ))
        db.create_unique('appli_socle_profilrecherche_type_d_offre', ['profilrecherche_id', 'typeoffre_id'])

        # Adding M2M table for field recherches_sauvegardees on 'ProfilRecherche'
        db.create_table('appli_socle_profilrecherche_recherches_sauvegardees', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('profilrecherche', models.ForeignKey(orm['appli_socle.profilrecherche'], null=False)),
            ('recherchesauvegardee', models.ForeignKey(orm['appli_socle.recherchesauvegardee'], null=False))
        ))
        db.create_unique('appli_socle_profilrecherche_recherches_sauvegardees', ['profilrecherche_id', 'recherchesauvegardee_id'])

        # Adding M2M table for field annonces_sauvegardees on 'ProfilRecherche'
        db.create_table('appli_socle_profilrecherche_annonces_sauvegardees', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('profilrecherche', models.ForeignKey(orm['appli_socle.profilrecherche'], null=False)),
            ('annonce', models.ForeignKey(orm['appli_socle.annonce'], null=False))
        ))
        db.create_unique('appli_socle_profilrecherche_annonces_sauvegardees', ['profilrecherche_id', 'annonce_id'])

        # Adding model 'EmailModele'
        db.create_table('appli_socle_emailmodele', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nom', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('sujet', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('texte', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('appli_socle', ['EmailModele'])

        # Adding model 'Alerte'
        db.create_table('appli_socle_alerte', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('modele_email', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appli_socle.EmailModele'])),
            ('semaine_envoi', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('appli_socle', ['Alerte'])

        # Adding model 'UtilisateurCAS'
        db.create_table('appli_socle_utilisateurcas', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='utilisateur_cas', unique=True, to=orm['auth.User'])),
            ('identifiant', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('domain', self.gf('django.db.models.fields.CharField')(default='dauphine', max_length=128)),
        ))
        db.send_create_signal('appli_socle', ['UtilisateurCAS'])

        # Adding unique constraint on 'UtilisateurCAS', fields ['identifiant', 'domain']
        db.create_unique('appli_socle_utilisateurcas', ['identifiant', 'domain'])

        # Adding model 'Parametre'
        db.create_table('appli_socle_parametre', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nom', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('valeur', self.gf('picklefield.fields.PickledObjectField')()),
            ('valeur_email', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appli_socle.EmailModele'], null=True, on_delete=models.PROTECT, blank=True)),
        ))
        db.send_create_signal('appli_socle', ['Parametre'])

    def backwards(self, orm):
        # Removing unique constraint on 'UtilisateurCAS', fields ['identifiant', 'domain']
        db.delete_unique('appli_socle_utilisateurcas', ['identifiant', 'domain'])

        # Deleting model 'TypeOffre'
        db.delete_table('appli_socle_typeoffre')

        # Deleting model 'TypeDePrestation'
        db.delete_table('appli_socle_typedeprestation')

        # Deleting model 'ProfilOffre'
        db.delete_table('appli_socle_profiloffre')

        # Deleting model 'Prestation'
        db.delete_table('appli_socle_prestation')

        # Deleting model 'Zone'
        db.delete_table('appli_socle_zone')

        # Deleting model 'Annonce'
        db.delete_table('appli_socle_annonce')

        # Removing M2M table for field zones on 'Annonce'
        db.delete_table('appli_socle_annonce_zones')

        # Removing M2M table for field prestations on 'Annonce'
        db.delete_table('appli_socle_annonce_prestations')

        # Deleting model 'RechercheSauvegardee'
        db.delete_table('appli_socle_recherchesauvegardee')

        # Deleting model 'ProfilRecherche'
        db.delete_table('appli_socle_profilrecherche')

        # Removing M2M table for field type_d_offre on 'ProfilRecherche'
        db.delete_table('appli_socle_profilrecherche_type_d_offre')

        # Removing M2M table for field recherches_sauvegardees on 'ProfilRecherche'
        db.delete_table('appli_socle_profilrecherche_recherches_sauvegardees')

        # Removing M2M table for field annonces_sauvegardees on 'ProfilRecherche'
        db.delete_table('appli_socle_profilrecherche_annonces_sauvegardees')

        # Deleting model 'EmailModele'
        db.delete_table('appli_socle_emailmodele')

        # Deleting model 'Alerte'
        db.delete_table('appli_socle_alerte')

        # Deleting model 'UtilisateurCAS'
        db.delete_table('appli_socle_utilisateurcas')

        # Deleting model 'Parametre'
        db.delete_table('appli_socle_parametre')

    models = {
        'appli_socle.alerte': {
            'Meta': {'object_name': 'Alerte'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modele_email': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['appli_socle.EmailModele']"}),
            'semaine_envoi': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'appli_socle.annonce': {
            'Meta': {'object_name': 'Annonce'},
            'date_de_creation': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'derniere_publication': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'derniere_validation': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'est_publiee': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monnaie': ('django.db.models.fields.CharField', [], {'default': "'EUR'", 'max_length': '3'}),
            'nombre_de_pieces': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'pays': ('django.db.models.fields.CharField', [], {'default': "'FR'", 'max_length': '3'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'position_geographique': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'prestations': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['appli_socle.Prestation']", 'symmetrical': 'False', 'blank': 'True'}),
            'prix_par_mois': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'proprietaire': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'annonces'", 'to': "orm['appli_socle.ProfilOffre']"}),
            'surface_en_m2': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'type_d_offre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['appli_socle.TypeOffre']"}),
            'validation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ville': ('django.db.models.fields.CharField', [], {'default': "'Paris'", 'max_length': '64'}),
            'zones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['appli_socle.Zone']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'appli_socle.emailmodele': {
            'Meta': {'object_name': 'EmailModele'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'sujet': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'texte': ('django.db.models.fields.TextField', [], {})
        },
        'appli_socle.parametre': {
            'Meta': {'object_name': 'Parametre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'valeur': ('picklefield.fields.PickledObjectField', [], {}),
            'valeur_email': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['appli_socle.EmailModele']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'})
        },
        'appli_socle.prestation': {
            'Meta': {'object_name': 'Prestation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type_de_prestation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['appli_socle.TypeDePrestation']"}),
            'valeur_numerique': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'appli_socle.profiloffre': {
            'Meta': {'object_name': 'ProfilOffre'},
            'acceptation_cgu': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'acceptation_charte_qualite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accepte_notif_alertes': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accepte_notif_depublication': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sans_validation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        'appli_socle.profilrecherche': {
            'Meta': {'object_name': 'ProfilRecherche'},
            'acceptation_cgu': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'annonces_sauvegardees': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['appli_socle.Annonce']", 'symmetrical': 'False', 'blank': 'True'}),
            'recherches_sauvegardees': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['appli_socle.RechercheSauvegardee']", 'symmetrical': 'False', 'blank': 'True'}),
            'type_d_offre': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['appli_socle.TypeOffre']", 'symmetrical': 'False', 'blank': 'True'}),
            'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        'appli_socle.recherchesauvegardee': {
            'Meta': {'object_name': 'RechercheSauvegardee'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intra_muros': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prix_maximum': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'quadrant': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        'appli_socle.typedeprestation': {
            'Meta': {'object_name': 'TypeDePrestation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'ordre': ('django.db.models.fields.IntegerField', [], {}),
            'type_de_valeur': ('django.db.models.fields.CharField', [], {'default': "'B'", 'max_length': '1'})
        },
        'appli_socle.typeoffre': {
            'Meta': {'object_name': 'TypeOffre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'appli_socle.utilisateurcas': {
            'Meta': {'unique_together': "(('identifiant', 'domain'),)", 'object_name': 'UtilisateurCAS'},
            'domain': ('django.db.models.fields.CharField', [], {'default': "'dauphine'", 'max_length': '128'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifiant': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'utilisateur_cas'", 'unique': 'True', 'to': "orm['auth.User']"})
        },
        'appli_socle.zone': {
            'Meta': {'object_name': 'Zone'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'polygon': ('django.contrib.gis.db.models.fields.PolygonField', [], {})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['appli_socle']