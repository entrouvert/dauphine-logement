# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'ProfilOffre.departement'
        db.delete_column(u'appli_socle_profiloffre', 'departement')

        # Adding field 'ProfilOffre.code_postal'
        db.add_column('appli_socle_profiloffre', 'code_postal',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=16, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'ProfilOffre.departement'
        db.add_column(u'appli_socle_profiloffre', 'departement',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=64, blank=True),
                      keep_default=False)

        # Deleting field 'ProfilOffre.code_postal'
        db.delete_column('appli_socle_profiloffre', 'code_postal')


    models = {
        'appli_socle.annonce': {
            'Meta': {'ordering': "('etat_validation', 'derniere_publication')", 'object_name': 'Annonce'},
            'date_de_creation': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'derniere_publication': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'derniere_validation': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'derniere_validation_par': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'duree_location': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'etat_validation': ('django.db.models.fields.CharField', [], {'default': "'attend_validation'", 'max_length': '32', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monnaie': ('django.db.models.fields.CharField', [], {'default': "'EUR'", 'max_length': '3'}),
            'nos_logements': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'pays': ('django.db.models.fields.CharField', [], {'default': "'FR'", 'max_length': '3'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'position_geographique': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'prestations': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['appli_socle.Prestation']", 'symmetrical': 'False', 'blank': 'True'}),
            'prix_par_mois': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2', 'db_index': 'True'}),
            'proprietaire': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'annonces'", 'to': "orm['appli_socle.ProfilOffre']"}),
            'raison_invalidation': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'surface_en_m2': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'type_d_offre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['appli_socle.TypeOffre']", 'on_delete': 'models.SET_DEFAULT'}),
            'type_de_logement': ('django.db.models.fields.CharField', [], {'default': "'studio'", 'max_length': '16'}),
            'ville': ('django.db.models.fields.CharField', [], {'default': "'Paris'", 'max_length': '64'}),
            'zones': ('django.db.models.fields.related.ManyToManyField', [], {'db_index': 'True', 'to': "orm['appli_socle.Zone']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'appli_socle.emailmodele': {
            'Meta': {'object_name': 'EmailModele'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'sujet': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'texte': ('django.db.models.fields.TextField', [], {})
        },
        'appli_socle.parametre': {
            'Meta': {'object_name': 'Parametre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'valeur': ('picklefield.fields.PickledObjectField', [], {}),
            'valeur_email': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['appli_socle.EmailModele']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'})
        },
        'appli_socle.prestation': {
            'Meta': {'object_name': 'Prestation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type_de_prestation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['appli_socle.TypeDePrestation']"}),
            'valeur_numerique': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'appli_socle.profiloffre': {
            'Meta': {'object_name': 'ProfilOffre'},
            'acceptation_cgu': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'acceptation_charte_qualite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accepte_invitations': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accepte_notif_alertes': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accepte_notif_depublication': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'code_postal': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16', 'blank': 'True'}),
            'comment': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'blank': 'True'}),
            'sans_validation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url_de_contact': ('django.db.models.fields.URLField', [], {'max_length': '1024', 'blank': 'True'}),
            'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'primary_key': 'True'}),
            'ville': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'blank': 'True'})
        },
        'appli_socle.profilrecherche': {
            'Meta': {'object_name': 'ProfilRecherche'},
            'acceptation_cgu': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'acceptation_charte_qualite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'annonces_sauvegardees': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'sauvegardee_par'", 'blank': 'True', 'to': "orm['appli_socle.Annonce']"}),
            'recherches_sauvegardees': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['appli_socle.RechercheSauvegardee']", 'symmetrical': 'False', 'blank': 'True'}),
            'type_d_offre': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['appli_socle.TypeOffre']", 'symmetrical': 'False', 'blank': 'True'}),
            'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        'appli_socle.recherchesauvegardee': {
            'Meta': {'object_name': 'RechercheSauvegardee'},
            'duree_location': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intra_muros': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'montant_loyer': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'prix_maximum': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'types_de_logement': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'zones': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['appli_socle.Zone']", 'null': 'True', 'blank': 'True'})
        },
        'appli_socle.typedeprestation': {
            'Meta': {'ordering': "('ordre', 'nom')", 'object_name': 'TypeDePrestation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'ordre': ('django.db.models.fields.IntegerField', [], {}),
            'type_de_valeur': ('django.db.models.fields.CharField', [], {'default': "'B'", 'max_length': '1'})
        },
        'appli_socle.typeoffre': {
            'Meta': {'object_name': 'TypeOffre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'appli_socle.utilisateurcas': {
            'Meta': {'unique_together': "(('identifiant', 'domain'),)", 'object_name': 'UtilisateurCAS'},
            'domain': ('django.db.models.fields.CharField', [], {'default': "'dauphine'", 'max_length': '128'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifiant': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'utilisateur_cas'", 'unique': 'True', 'to': "orm['auth.User']"})
        },
        'appli_socle.zone': {
            'Meta': {'object_name': 'Zone'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'polygon': ('django.contrib.gis.db.models.fields.PolygonField', [], {})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['appli_socle']