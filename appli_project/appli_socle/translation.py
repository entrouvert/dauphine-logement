from modeltranslation.translator import translator, TranslationOptions

from models import TypeDePrestation, TypeOffre

class TypeDePrestationTranslationOptions(TranslationOptions):
    fields = ('nom',)

class TypeOffreTranslationOptions(TranslationOptions):
    fields = ('nom',)

translator.register(TypeDePrestation, TypeDePrestationTranslationOptions)
translator.register(TypeOffre, TypeOffreTranslationOptions)
