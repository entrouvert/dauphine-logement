window.CKEDITOR_CONFIG_FORMAT_TAGS = 'p;h1;h2;h3;h4;h5';
window.CKEDITOR_CONFIG_STYLES_SET = [
        { name: 'Paragraphe centré', element: 'p', attributes: { 'class': 'centered' } },
        { name: 'Encadré', element: 'p', attributes: { 'class': 'framed' } },
];
