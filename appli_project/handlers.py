import logging.handlers
import socket

class SyslogHandler(logging.handlers.SysLogHandler):
    def emit(self, record):
        """
        Emit a record.

        The record is formatted, and then sent to the syslog server. If
        exception information is present, it is NOT sent to the server.
        """
        msg = self.format(record)
        """
        We need to convert record level to lowercase, maybe this will
        change in the future.
        """
        source_msg = msg
        i = 0
        while source_msg[i:]:
            msg = source_msg[i:i+900]
            i += 900
            msg = self.log_format_string % (
                self.encodePriority(self.facility,
                                    self.mapPriority(record.levelname)),
                                    msg)
            try:
                if self.unixsocket:
                    try:
                        self.socket.send(msg)
                    except socket.error:
                        self._connect_unixsocket(self.address)
                        self.socket.send(msg)
                else:
                    self.socket.sendto(msg, self.address)
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                self.handleError(record)

