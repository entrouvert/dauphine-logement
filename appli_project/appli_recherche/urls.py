# vim:spell:spelllang=fr
# -*- encoding: utf-8 -*-

from django.conf.urls.defaults import patterns, url

import views

urlpatterns = patterns('',
        url(r'^$', views.Accueil.as_view(), name='recherche'),
        url(r'^connexion/$', views.Connexion.as_view(),
            name='recherche-connexion'),
        url(r'^mes-alertes/$', views.MesAlertes.as_view(), name='mes-alertes'),
        url(r'^mes-annonces/$', views.MesAnnonces.as_view(),
            name='mes-annonces'),
        url(r'^(?P<pk>\d+)/$', views.Annonce.as_view(),
            name='recherche-annonce'),
        url(r'^(?P<pk>\d+)/contact/$', views.Contact.as_view(),
            name='annonce-contact'),
    )
