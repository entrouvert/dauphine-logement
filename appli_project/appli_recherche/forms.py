# vim:spell:spelllang=fr
# -*- encoding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
import django.forms as forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, ButtonHolder, Submit, Fieldset, Field
from django.core.exceptions import ValidationError

from appli_project.appli_socle.models import QUADRANTS, TYPE_LOGEMENT, DUREE_LOCATION

class ConnexionEntForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(
                    ButtonHolder(
                        Submit('compte-ent', _(u'Me connecter'))))
        super(ConnexionEntForm, self).__init__(*args, **kwargs)

class RaccordementCompteEntForm(forms.Form):
    pass

class ContactAnnonceForm(forms.Form):
    message = forms.CharField(widget=forms.Textarea)

    helper = FormHelper()
    helper.form_id = 'contact-form'
    helper.form_class = 'form-horizontal'
    helper.layout = Layout(
            Field('message', placeholder=_(u'Écrivez ici le message destiné au '
                u'propriétaire. Votre email lui sera automatiquement rappelé, '
                u'mais vous pouvez lui donner par contre un numéro de téléphone')),
            ButtonHolder(
                Submit('contacter', _(u'Contacter'), css_class='btn btn-primary'),
                Submit('annuler', _(u'Annuler'), css_class='btn'),
                css_class='bouton-group'))

def validateur_positif(value):
    if value < 0:
        raise ValidationError('Le prix maximum doit être positif')

MONTANT_LOYER = (
        ('', '---'),
        (0, _(u'moins de 500 euros')),
        (1, _(u'entre 500 et 700 euros')),
        (2, _(u'plus de 700 euros')))

class BaseRechercheForm(forms.Form):
    type_de_logement = forms.MultipleChoiceField(choices=TYPE_LOGEMENT,
            label=_(u'Type de logement'), required=False,
            widget=forms.CheckboxSelectMultiple)
    montant_loyer = forms.TypedChoiceField(
            label=_(u'Montant du loyer'),
            help_text=_(u'par mois'),
            choices=MONTANT_LOYER,
            coerce=int,
            required=False,
            empty_value=None)
    duree_location = forms.TypedChoiceField(
            label=_(u'Durée de location'),
            choices=(('', '---'),) + DUREE_LOCATION,
            coerce=int,
            empty_value=None,
            required=False)
    paris_et_alentours = forms.ChoiceField(
            label='',
            choices=(('paris', _(u'Paris')),
                ('paris-et-alentours', _(u'Paris et banlieue parisienne'))),
            widget=forms.RadioSelect, required=False)
    quadrants = forms.MultipleChoiceField(
            label='',
            choices=QUADRANTS,
            widget=forms.CheckboxSelectMultiple,
            required=False)

    def __init__(self, *args, **kwargs):
        initial = kwargs.setdefault('initial', {}).copy()
        kwargs['initial'] = initial
        initial['type_de_logement'] = [a for a,b in TYPE_LOGEMENT]
        super(BaseRechercheForm, self).__init__(*args, **kwargs)

class AlerteForm(BaseRechercheForm):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(
                Fieldset(_(u'Créer une alerte'),
                    'type_de_logement',
                    'montant_loyer',
                    'duree_location',
                    Fieldset(_(u'Position géographique'),
                        'paris_et_alentours',
                        'quadrants',
                        css_class='position_geographique'),
                    ButtonHolder(
                        Submit('submit', _(u'Ajouter une nouvelle alerte')))))
        super(AlerteForm, self).__init__(*args, **kwargs)

class RechercheForm(BaseRechercheForm):
    def __init__(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'GET'
        self.helper.form_id = 'filtre-annonces'
        if request:
            self.helper.form_action = request.get_full_path() + '#filtre-annonce'
        self.helper.layout = Layout(
                Fieldset(_(u'Filtrer les annonces'),
                    'type_de_logement',
                    'montant_loyer',
                    'duree_location',
                    Fieldset(_(u'Position géographique'),
                        'paris_et_alentours',
                        'quadrants',
                        css_class='position_geographique'),
                    ButtonHolder(
                        Submit('submit', _(u'Filtrer')),
                        Submit('sauvegarder', _(u'Sauvegarder comme alerte')),
                        css_class='bouton-group')))
        super(RechercheForm, self).__init__(*args, **kwargs)
