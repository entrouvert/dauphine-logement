# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'ContentItem.name_fr'
        db.add_column(u'fiber_contentitem', 'name_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ContentItem.name_en'
        db.add_column(u'fiber_contentitem', 'name_en',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ContentItem.content_markup_fr'
        db.add_column(u'fiber_contentitem', 'content_markup_fr',
                      self.gf('fiber.utils.fields.FiberMarkupField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'ContentItem.content_markup_en'
        db.add_column(u'fiber_contentitem', 'content_markup_en',
                      self.gf('fiber.utils.fields.FiberMarkupField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'ContentItem.content_html_fr'
        db.add_column(u'fiber_contentitem', 'content_html_fr',
                      self.gf('fiber.utils.fields.FiberHTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'ContentItem.content_html_en'
        db.add_column(u'fiber_contentitem', 'content_html_en',
                      self.gf('fiber.utils.fields.FiberHTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Page.title_fr'
        db.add_column(u'fiber_page', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Page.title_en'
        db.add_column(u'fiber_page', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)
        for page in orm.Page.objects.all():
            page.title_fr = page.title
            page.title_en = page.title
            page.save()
        for content_item in orm.ContentItem.objects.all():
            content_item.content_markup_fr = content_item.content_markup
            content_item.content_html_fr = content_item.content_html
            content_item.save()


    def backwards(self, orm):
        # Deleting field 'ContentItem.name_fr'
        db.delete_column(u'fiber_contentitem', 'name_fr')

        # Deleting field 'ContentItem.name_en'
        db.delete_column(u'fiber_contentitem', 'name_en')

        # Deleting field 'ContentItem.content_markup_fr'
        db.delete_column(u'fiber_contentitem', 'content_markup_fr')

        # Deleting field 'ContentItem.content_markup_en'
        db.delete_column(u'fiber_contentitem', 'content_markup_en')

        # Deleting field 'ContentItem.content_html_fr'
        db.delete_column(u'fiber_contentitem', 'content_html_fr')

        # Deleting field 'ContentItem.content_html_en'
        db.delete_column(u'fiber_contentitem', 'content_html_en')

        # Deleting field 'Page.title_fr'
        db.delete_column(u'fiber_page', 'title_fr')

        # Deleting field 'Page.title_en'
        db.delete_column(u'fiber_page', 'title_en')


    models = {
        u'fiber.contentitem': {
            'Meta': {'object_name': 'ContentItem'},
            'content_html': ('fiber.utils.fields.FiberHTMLField', [], {}),
            'content_html_en': ('fiber.utils.fields.FiberHTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_html_fr': ('fiber.utils.fields.FiberHTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_markup': ('fiber.utils.fields.FiberMarkupField', [], {}),
            'content_markup_en': ('fiber.utils.fields.FiberMarkupField', [], {'null': 'True', 'blank': 'True'}),
            'content_markup_fr': ('fiber.utils.fields.FiberMarkupField', [], {'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metadata': ('fiber.utils.json.JSONField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'protected': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'template_name': ('django.db.models.fields.CharField', [], {'max_length': '70', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'used_on_pages_data': ('fiber.utils.json.JSONField', [], {'null': 'True', 'blank': 'True'})
        },
        u'fiber.file': {
            'Meta': {'ordering': "('file',)", 'object_name': 'File'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'fiber.image': {
            'Meta': {'ordering': "('image',)", 'object_name': 'Image'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'height': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'fiber.page': {
            'Meta': {'ordering': "('tree_id', 'lft')", 'object_name': 'Page'},
            'content_items': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['fiber.ContentItem']", 'through': u"orm['fiber.PageContentItem']", 'symmetrical': 'False'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'mark_current_regexes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'metadata': ('fiber.utils.json.JSONField', [], {'null': 'True', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'subpages'", 'null': 'True', 'to': u"orm['fiber.Page']"}),
            'protected': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'redirect_page': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'redirected_pages'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['fiber.Page']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'show_in_menu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'template_name': ('django.db.models.fields.CharField', [], {'max_length': '70', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'url': ('fiber.utils.fields.FiberURLField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'fiber.pagecontentitem': {
            'Meta': {'object_name': 'PageContentItem'},
            'block_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'page_content_items'", 'to': u"orm['fiber.ContentItem']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'page_content_items'", 'to': u"orm['fiber.Page']"}),
            'sort': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['fiber']
