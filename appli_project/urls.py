# vim:spell:spelllang=fr
# -*- encoding: utf-8 -*-

from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

import appli_project.appli_socle.urls
import appli_project.appli_offre.urls
import appli_project.appli_recherche.urls


urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),

    # i18n switcher
    (r'^i18n/', include('django.conf.urls.i18n')),

    # fiber
    url(r'^api/v2/', include('fiber.rest_api.urls')),
    url(r'^jsi18n/$', 'django.views.i18n.javascript_catalog', {'packages': ('fiber',),}),
    url(r'^admin/fiber/', include('fiber.admin_urls')),
    # admin
    url(r'^admin/', include(admin.site.urls)),
    # offre
    url(r'^offre/', include(appli_project.appli_offre.urls)),
    # recherche
    url(r'^recherche/', include(appli_project.appli_recherche.urls)),
    # application
    url(r'^', include(appli_project.appli_socle.urls)),
    # fiber integration :
    url(r'', 'fiber.views.page'),
)
